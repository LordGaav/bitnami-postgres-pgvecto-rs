ARG PGVECTORS_TAG
ARG BITNAMI_TAG

FROM tensorchord/pgvecto-rs-binary:${PGVECTORS_TAG} as binary
FROM docker.io/bitnami/postgresql:${BITNAMI_TAG}

COPY --from=binary /pgvecto-rs-binary-release.deb /tmp/vectors.deb
USER root
RUN dpkg -i /tmp/vectors.deb \
 && rm -f /tmp/vectors.deb \
 && mv /usr/lib/postgresql/*/lib/vectors.so /opt/bitnami/postgresql/lib/ \
 && mv usr/share/postgresql/*/extension/vectors* opt/bitnami/postgresql/share/extension/

USER 1001
ENV POSTGRESQL_EXTRA_FLAGS="-c shared_preload_libraries=vectors.so"
